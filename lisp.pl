package Patmat;
use strict;
use warnings;
no warnings 'once';

use Data::Dumper;
use Scalar::Util qw(looks_like_number);
use YAML::XS;

sub  _ {  \'__DEFAULTMATCH__'};

my $parser;

    
our $Cons = '__CONS__';
our $Indentation = 3;

our $M ; # matrizes

sub number_checkpair {
    
    my $val_checker = sub {
        my ($val) = (@_);
        if(ref \$val eq 'SCALAR'){
            return (looks_like_number($val)) ? undef : 
               "Err: value is not a number" ;
        }else{
            return 'Err: val is not a scalar'
        }
    };
    my $pat_checker = sub {
        my ($pat) = @_;
        my $pattype = ref $pat;
        if($pattype eq 'ARRAY'){
            return undef
        }elsif(\$pat eq 'SCALAR'){
            return undef
        }else{
            return "Err: not a valid pattern for numbers"
        }
    };

    return ($val_checker, $pat_checker);
}

our $Id = {
    withblock => \'withblock' ,
    numberblock => \&number_checkpair, 
    caseblock => \'caseblock' ,
    matrix => \'matrix',
    number_checker => 'NUMBER_CHECKER'
};

sub num{
    @_
}

sub with (&){
    return ($Id->{withblock}, @_);
}
sub number (&){
    return (\&number_checkpair, @_);
}

sub c {

    my $conser; $conser = sub {
        if(@_ == 0 ){
            return bless [ ], $Cons 
        }elsif(@_ == 1 ){
            return bless [ $_[0]], $Cons
        }elsif(@_ == 2 ){
            return bless [ $_[0], $_[1] ], $Cons 
        }else{
            return bless [ shift,  $conser->(@_) ], $Cons 
        }
    };
    $conser->(@_)
}

sub rtmatch_scalar {
    my ($val, $pat) = @_;

    if(defined $pat  ){
        if (looks_like_number($val)){ # not reliable for stringified numbers due to  perl-bug
            # die "Err: val is not a number" unless looks_like_number $val;
            ($val == $pat) ? 1 : undef
        }else{
            ($val eq $pat) ? 1 : undef
        }
    }else{
        return undef
    } 
}

sub rtmatch_glob{
    die "tdo"
}


sub rtmatch_undef {
    my ($val, $pat) = @_;
    
    (defined $pat)
        ? undef
        : 1;
}


sub rtmatch_dispatcher {
    my ($val) = @_;
    my $type = ref $val;
    my $case =  {
        '' => sub { 
                if(defined $val  ){
                    ( ref \$val eq 'SCALAR' ) ? \&rtmatch_scalar : \&rtmatch_glob
                }else{
                    \&rtmatch_undef
                } },
        _ => sub { die "err " }
    };
    
    (exists $case->{$type} ) 
        ? $case->{$type}->()
        : $case->{_}->();
}

sub glob_wrapper{
    my $glob = shift;
    my %locenv; 
    if(ref \$glob eq 'GLOB'){
         $glob =~  m/main::(.*)/ ;
         my $catch = $1;
         return sub {
             my ($vars) = shift;
             (exists $vars->{$catch}) 
                ? $vars->{$catch}
                : die "Err: variable $catch is not available"
            };
    }else{
        die "var is not a glob"
    }

}

sub base_wrapper {
    my $base_val = shift;
    return sub { $base_val }
}

sub action_wrapper {
    my $action = shift;
    my $type = ref $action;

    my $case =  {
        'CODE' => sub { $action },
        '' => sub { return ( ref \$action eq 'GLOB' ) 
                        ? glob_wrapper($action) 
                        : base_wrapper($action) },
        _ => sub { die "err " }
    };
    
    (exists $case->{$type} ) 
        ? $case->{$type}->()
        : $case->{_}->();

}


sub multimatch {
        die "tdo multimatch"
}
sub exec_action{
    my ($vars, $action ) = @_;
    return $action->($vars);
}
sub domatch {
    my ($vars, $matrix) = (shift, shift);

    if (@_ == 0){
        return $matrix->{default};
    }elsif(@_ == 1){
        my $action =  singlematch($matrix, @_);
        exec_action($vars, $action )
    }else{
        return multimatch($vars, $matrix,  @_)
    }


}
sub casematch_matrix {
    my $block = shift;
    my @parts = $block->();
    my @rows ;
    my $default;
    for (my $i = 0 ;  $i < @parts ; $i= $i + 2){
        die 'xx ' . _;
        if($parts[$i] eq _ ){
            $default = $parts[ $i + 1 ]
        }else{
            push @rows, [ $parts[$i], action_wrapper($parts[$i+1] ) ] 
        }
    }
    if($default){
            return bless { default => action_wrapper($default), rows => \@rows }, 'MATCHMATRIX'
    }else{ 
        die "Err: match matrix not correct"
    }

}

sub parm_env {

    my @vars;
    foreach  (@_) {
        if(ref \$_ eq 'GLOB'){
              m/main::(.*)/ ;
              push @vars, $1;
        }else{
            die "var is not a glob"
        }
    }

}
sub dynfun_ast {
    my ($parm, $blockid, $block) = @_;

    my $parmdata = Load "[ $parm ]";

    print Dumper $parmdata;

    my $parm_env = parm_env(@$parmdata);

    exit;

    my @vars;
    my $matrix;
    if(ref $matrix eq 'MATCHMATRIX'){
        return sub { 
            my @args = @_;
            my %varsenv ;
            foreach (@vars){
                $varsenv{$_} = shift @args
            }

            domatch( \%varsenv, $matrix, @_) };
    }else{
        die 'tdo fun'
    }
    return sub{};

    if($blockid eq $Id->{caseblock}){
        return casematch_matrix($block);
    }else{
        die "Err: dynfun_ast todo"
    }
}

sub fun {

    my $id = join '-',  caller();

    if(exists $M->{$id} ){
        return $M->{$id}
    }else{
        if(@_ == 3 ) {
            my ($parmstr, $matrixid, $matrix) = @_;
            die "Err(fun): invailid arg" unless (ref \$parmstr eq 'SCALAR');
            die "Err(fun): invailid arg" unless ($matrixid  eq $Id->{matrix});
            die "Err(fun): invalid arg" unless (ref $matrix  eq 'CODE');
            my $ast = dynfun_ast(@_);
            $M->{$id} = $ast;
            print Dumper $M; 
        }else{
            die 'Err(fun) invalid args'
        }
    }

    return ;
}


sub buildmatrix {
    my (@rows , $default);
    for (my $i = 0 ;  $i < @_ ; $i= $i + 2){
        if($_[$i] eq _ ){
            $default = $_[ $i + 1 ]
        }else{
            #push @rows, [ $_[$i], action_wrapper($_[$i+1] ) ] 
            push @rows, [ $_[$i], $_[$i+1]  ] 
        }
    }

    if($default){
        return  {
            default => $default,
            rows => \@rows
        };
    }else{ 
        die "Err: match matrix not correct"
    }

}
sub lex_block {
    my ($typechecker, $block) = @_; 


    if(grep { $typechecker eq $_ } ( $Id->{withblock}, $Id->{numberblock} ) ){
        my @parts = $block->();
        my $size = @parts;
        my $r = $size % 2 ;
        if($r){
            return (undef, "Err: invalid number of patterns")
        }else{
            my $default =  ($parts[$size -2 ]  );
            if($default eq _() ){
                my $ast;
                #my $ast = buildmatrix @parts;
                #$ast->{checker} = $typechecker;
                return $ast;
            }else{
                return (undef, "Err: no default pattern")
            }
        }
    }else{
        return (undef, "Err: invalid with block data " );
    }

}
sub do_match {
    my ($matrix, $val) = @_;

    if($val){
        my $val_matcher = rtmatch_dispatcher($val);
        my $ret;
        foreach my $row (@{ $matrix->{rows}} ){
            my $res = $val_matcher->($val, $row->[0]);
            if($res){
                $ret = $row->[1];
                last;
            }
        }
        exit;
        return ($ret ) ? $ret->({}, $val) : $matrix->{default};
    }else{
        return $matrix->{default};
    }

}

sub parse_block {
}
sub match {
    my $val = $_[0];

    my $id = join '-',  caller();
    my $ast ;
    if(exists $M->{$id} ){
        $ast = $M->{$id}
    }else{
        die "Err: invalid match expression" unless (@_ == 3 );
        my $err;
        my ($typechecker, $block) = ($_[1], $_[2] );
        ($ast, $err) = lex_block($block);
        if($err){
           die 'Err: invalid match block ' . $err
        }else{
            #my $compblock = parse_block($matrix);
            $M->{$id} = $ast;
       }
    }
    my $m =  do_match($ast, $val);
    return $m->();
}



# (( )))())
my %P = qw| 
    ) (
    ] [ 
    } {
|;

sub lexer {

    my $lines = shift;

    my (@buffer, @pstk, @tks,  $in, );
    my $mktokenp = sub {
        #'(' => [ $mktokenp, '('] , ')' => [ $mktokenp, ')'],  
        if($_[1]){
            if(@buffer){ 
                push @tks, { $_[1] => join '', @buffer} ; ;
            }else{
                die "fuck"
            }
        }else{
            if($_[2]){
                if(@buffer){
                    die "Err: syntax err";
                }
            }else{
                if(@buffer){ 
                    push @tks, join '', @buffer; undef @buffer; 
                }else{ }; 
            }
        }
        if($_[0]){ # paren
            if(exists $P{$_[0]}){
                my $c = pop @pstk;
                my $p = $P{$_[0]};
                unless($c ){ die "Err: paren mismatch" }
                unless($c eq $p ){ die "Err: paren mismatch" }
            }else{
                push @pstk, $_[0];
            } 
            push @tks, $_[0];
        }else{
            #' ' => [ $mktokenpp , undef, 'fd', '<' ] , 
            if($_[2]){ 
                if(@buffer){
                    if($_[1]){
                        undef @buffer;
                    }else{
                        die 'suck';
                    }

                }else{
                    push @tks, $_[2]
                    
                }
            }else{
                die 'su';
            }

       }

        undef $in;
        };
    my $mkspecial = sub {
            if(@buffer){ 
                push @tks, { $_[0] => join '', @buffer} ; undef @buffer; }
                undef $in;
            }; 
    my $mktoken = sub {
            if(@buffer){ push @tks, join '', @buffer; undef @buffer; }
            if($_[0]) { $in = $_[0]}
        }; 
    my %pidge = qw | * _STAR_ | ;
    my $mktokeno = sub {
            if(@buffer){ 
                push @buffer, $pidge{$_[1]} 
        }else{
            if($_[0]) { $in = $_[0]}
        }
    }; 
    my %strcase = (
        '"' => [ $mkspecial , 'str'] , 
        (_) => [ sub { push @buffer, $_[0] } ] 
       );
    my %fdcase = (
        '>' => [ $mkspecial , 'fd'] , 
        ' ' => [ $mktokenp , undef, undef,  '<'] , 
        (_) => [ sub {
            if($_[0] =~ /[A-Za-z]+/){push @buffer, $_[0]}else{
                die "Err: no special chars in fd"} 
        }] 
       );
    my %starcase = ( 
        ' ' => [ $mktokenp , undef, 'glob',  '*' ] , 
        '(' => [ $mktokenp, '(', 'glob'] , ')' => [ $mktokenp, ')', 'glob'],  
        '[' => [ $mktokenp, '[', 'glob'], ']' => [ $mktokenp, ']', 'glob'], 
        '{' => [ $mktokenp, '{', 'glob'], '}' => [ $mktokenp, '}', 'glob'], 
        (_) => [ sub {
            if($_[0] =~ /[A-Za-z]+/){push @buffer, $_[0]}else{
                die "Err: no special chars in glob"} 
        }] 
    );
    my %case = ( 
        ' ' => [ $mktoken ] , 
        '"' => [ $mktoken , \%strcase] , 
        '*' => [ $mktokeno , \%starcase, '*'] , 
        '<' => [ $mktokeno , \%fdcase] , 
        '(' => [ $mktokenp, '('] , ')' => [ $mktokenp, ')'],  
        '[' => [ $mktokenp, '['], ']' => [ $mktokenp, ']'], 
        '{' => [ $mktokenp, '{'], '}' => [ $mktokenp, '}']
    );

    my $lnr = 0;
    foreach (@$lines) {
        $lnr++;
        push @tks, [ $lnr ] ; 
        foreach (split '', $_){
            if($in){
                if(exists $in->{$_}){
                    my ($clos, @args) = @{ $in->{$_} };
                    $clos->(@args);
                }else{
                    my ($clos, @args) = @{ $in->{(_)} };
                    $clos->($_);
                }
            }else{
                if(exists $case{$_}){
                    my ($clos, @args) = @{ $case{$_} };
                    $clos->(@args);
                }else{
                    push @buffer, $_
                }
            }
        }
    }
    die "Err: paren error" if @pstk;
    return  \@tks;
}

# ( a ( c d ) g e)
#    <-[cd] 
#             [ge]
# [ a [cd] ] 
#
#   a [cd] g e 
# [ a [ cd] g 
#


sub mk_default {
    my ($ctx, $sym, $list) = @_;

    my $elist = $ctx->{l}->($ctx, $list);

    my $txt = join ', ' , @$elist;

    return  "(\$$sym->($txt))";
}
sub mk_func {
    my ($ctx, $sym, $list) = @_;

    if($list){
        my $elist = $ctx->{l}->($ctx, $list);
        my $txt = join ', ' , @$elist;
        return "$sym($txt)";
    }else{
        return "sub\{ $sym \@\_ \}"
    }

}
sub mk_def {
    my ($ctx, $sym, $list) = @_;

    if($list){
        my $elist = $ctx->{l}->($ctx, $list);
die 'ss ' . Dumper $list;
        my ($esym, @val);
        my $txt = join ', ' , @$elist;
    return "$sym($txt)";
    }else{
        die "Err: def is no a rvalue"
    }

}

sub mk_op2 {
    my ($ctx, $sym, $list) = @_;

    my $elist = $ctx->{l}->($ctx, $list);

    return (@$elist > 2 )
        ? die "Err: synatx error in op2" 
        : "($elist->[0] $sym $elist->[1] )"
}

sub mk_if {
    my ($ctx, $sym, $list) = @_;

    my $elist = $ctx->{l}->($ctx, $list);

    my ($cond, $if, $else); 
    if(@$elist == 3){
        ($cond, $if, $else) = @$elist;
    }elsif(@$elist == 2){
        ($cond, $if, $else) = (@$elist, '');
    }else{
        die "Err: if stmt"
    }
    return "(($cond)?($if):($else))";
}
sub mk_special {
    my ($ctx, $sym) = @_;
    return "$sym";

}
sub mk_loop {
    my ($ctx, $sym, $list) = @_;

    my $elist = $ctx->{l}->($ctx, $list);

    if(@$elist == 2){
        return "[ map { $elist->[1] } \@\{ $elist->[0] \} ]";
    }else{
        die "Err: loop syntax "
    }
}
sub mk_do {
    my ($ctx, $sym, $list) = @_;
    

    my $nctx = { %$ctx };
    $nctx->{ind} = $ctx->{ind} + $Indentation;

    my $ws = ' ' x ($nctx->{ind}  ); 

    my $elist = $ctx->{l}->($nctx, $list);

    my @block;
    foreach (@$elist) {
        push @block, ($ws . $_  . ';')
    }

    my $blocktxt = join "\n", @block;

    return "\{\n$blocktxt\n\}";

}
my %expr_forms = ( 
    1 => \&mk_func,
    2 => \&mk_op2,  
    10 => \&mk_if, 
    11 => \&mk_loop, 
    12 => \&mk_do, 
    13 => \&mk_def, 
    20 => \&mk_special, 

    _ => \&mk_default
);


my %expressions = qw |
    print 1
    > 2
    < 2
    == 2
    if 10
    loop 11
    do 12
    def 13
    $_ 20 
|;

sub mk_expr {
    my ($ctx, $list) = @_; 

    my $sym = shift @$list;


    return (exists $expressions{$sym}) 
        ? $expr_forms{($expressions{$sym})}->($ctx, $sym, $list)
        : $expr_forms{_}->($ctx, $sym, $list)

}
sub mk_vector {
    my ($ctx, $list) = @_; 


    my $elist = $ctx->{l}->($ctx, $list);
    my $txtlist = join ', ', @$elist;
    return "[ " . $txtlist . " ]";

}
sub mk_atom {
    my ($ctx, $sym) = @_;
    if (looks_like_number($sym)){
         return $sym
     }else{
        return (exists $expressions{$sym}) 
            ? $expr_forms{($expressions{$sym})}->($ctx, $sym)
            : "\$$sym";
     }

}

sub mk_string {
    my ($ctx, $string) = @_;
    return "\"$string\""
}
my %asts = ( 
    '(' => \&mk_expr,
    '{' => sub {},
    '[' => \&mk_vector,  
    str => \&mk_string
);



$parser = sub {
    my ($ctx, $list) = @_;

   my $hd = shift @$list;

   if(ref \$hd eq 'SCALAR'){
       if(grep $_ eq $hd,  qw| ( [ { | ){
           return  $asts{$hd}->($ctx, $list);
       }elsif ( grep { $_ eq $hd} qw| ) ] } | ){
           die "Err: Premature list end"
       }elsif(grep $_ eq $hd,  ( qw| ) ] } |  )){
       }else{
           return mk_atom($ctx, $hd)
       }
    }elsif( ref $hd eq 'ARRAY'){
        my $nctx = { %$ctx } ;
        ($nctx->{lnr}) = @$hd;
        $parser->($nctx, $list)
    }elsif( ref $hd eq 'HASH'){
        my ($k, $v ) =  %$hd;
        return $asts{$k}->($ctx, $v);
    }else{
        die 'fucker' . Dumper $hd;;

    }
};

sub lister {
    my ($ctx, $block ) = @_;

    my @l;
    while ( not (grep { $_ eq $block->[0]}  (qw| ) ] } |))) {
        push @l, $parser->($ctx, $block);
    }
    shift @$block;
    return \@l;
}

#my @tks =  lexer '((print 33 ) 33  (plus 08  ))';

my $ctx = { 
        lnr => 0, 
        ind => 0, 
        p => $parser,
        l => \&lister,
    };
my ($tks) =  lexer ( [ '( def (sdjf 3e ))' ] ); 
die 'dd ' . Dumper $tks;
my @res = $parser->($ctx, $tks);
print 'rr ' . Dumper @res;
#print 'rr ' . Dumper eval $res[0];

__END__
my @comp = $parse->();
print 'cc ' . Dumper @comp; 
#print Dumper $comp[0]->[0]->{'('};

exit;

match 33 , number {
    \'.. a 4' => "suck",
    '*a' => q(print *a) ,
    (_) => "fuck"
};


__END__
match c(33, 44) , with {
    c(*hd, *tl) => 44,
    (_) => 88 
};


my $res = match c(33), with {
    [ *a, *b ] => "fuck off",
    (_)   => "suck"
};

