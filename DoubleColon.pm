package DoubleColon;
use strict qw(subs vars);
use warnings;
no warnings 'once';

use Data::Dumper;
use Scalar::Util qw(looks_like_number);
use YAML::XS;


my $Defaultpattern  = \'__Defaultpattern';
use constant _ => $Defaultpattern;



sub  guard {
   return bless \@_, 'GUARD'
};

my $parser;

sub ::say {
    (@_)
        ? do {print @_ ; print "\n";} 
        : (print @_)
    ;
}
sub ::p {
    (@_)
        ? do {print @_ ; print "\n";} 
        : (print @_)
    ;
}
sub ::d {
    print Dumper @_;
}

my %ac_cases  = (
    num => sub { ((looks_like_number($_[0] ))? $_[0] : undef ) }, 
);
sub typechecking {
    my ($ctx, $type, $arg) = @_;

    
    if (ref $arg eq 'TYPE'){
        return (($type eq $arg->[1])
            ? $arg 
            : (undef,   "Err: wrong type "))
            ;
    }elsif ( ref \$arg eq 'GLOB'){     #tags
        $arg =~ m/\*[a-zA-Z]*\:\:([A-Z][a-z]*)/;
        return (($1 eq  $ctx->{tree}->{$1})
            ? ($1)
            : (undef, "Err: invalid type " . "'" . $1 . "'"))
            ;

    }else{
        return  (undef, "Err: type error ") 
    }

}
sub argchecking {
    my ($ctx, $parms )= (shift, shift);
    die "Err(typedef): argument mismatch " unless @$parms == @_;

    my @args;
    my $i = 0;
    foreach (@_){
        my ($res, $err) = (exists $ac_cases{$parms->[$i]}) 
            ? $ac_cases{$parms->[$i]}->($_)
            : typechecking($ctx, $parms->[$i], $_)
            ;
        ($res)
            ? (push @args, $res)
            : (die $err) 
            ;
        $i++;
    }
    return \@args;
}

sub ::type {
    die "Err(type): wrong syntax" unless @_ == 2;
    my ($root, $ydef) = @_;

    my $def = Load '[ ' . $ydef . ' ]';

    my %typetree ;

    my ($pkg, $file, $line) = caller();

    my $ctx = { pkg => $pkg, file => $file, line => $line,  tree => \%typetree };


    foreach (@$def){
        if(ref \$_ eq 'SCALAR'){
            $typetree{$_} = $_;
        }elsif (ref $_ eq 'HASH'){
            my ($name, $parms ) = %{ $_ };
            my $ctr =  sub { 
                bless [ $ctx, $root,  $name, argchecking($ctx, $parms, @_ ) ] , 'TYPE';
            };
            $typetree{$name} = $ctr;
            *{"main\::$name" } = $ctr; 
        }else{ 
            die "Err(type): syntax ";
        }
    }

    *{"main\::$root" } = sub { typechecking($ctx, 'color', $_[0])};
    $typetree{$root} = \%typetree;
    return ();
}


sub base_action {
    my ($action ) = @_;

    if(ref $action eq 'CODE'){
        return $action
    }elsif( ref \$action eq 'SCALAR'){
        return sub { return $action }
    }else {
        die "Err: invalid action"
    }
}

sub nc_gt {
    my ($l, $r) = @_;
    die "Err: missing left arg" unless $l;
    die "Err: missing right arg" unless $r;
    return  sub {  ($l > $r) }; 
}
sub nc_lt {
    my ($l, $r) = @_;
    die "Err: missing left arg" unless $l;
    die "Err: missing right arg" unless $r;
    return sub { ($l < $r) }, 
}
sub nc_eq {
    my ($l, $r) = @_;
    die "Err: missing left arg" unless $l;
    die "Err: missing right arg" unless $r;
    return  sub { ($l == $r) }, 
}
my %num_clauses = (
    '>' => \&nc_gt, 
    '<' => \&nc_lt,  
    '==' => \&nc_eq, 
);
sub num_pattern {
    my ($patt) = @_;
    if(ref \$patt eq 'SCALAR'){
        if($patt eq '_'){
            return sub { 1 };
        }else{
            die "todo num string"
        }
    }elsif(ref $patt eq 'GUARD'){
        my ($l, $op,  @rest) = @$patt;
        (exists $num_clauses{$op})
            ? $num_clauses{$op}->($l, @rest)
            :  die "Err: invalid num expr ";

    }elsif(ref $patt eq 'ARRAY'){
        my ($l, $op, $r) = @$patt;
        die "Err: invalid expr" if  (@$patt == 0); 
        
        (exists $num_clauses{$op})
            ? $num_clauses{$op}->( $l, $r )
            : die "Err: invalid num clause" . $op;
    }elsif(ref $patt eq 'CODE'){
        die "Err: todo " ; 
    }elsif(ref $patt eq 'SCALAR'){
        if($patt == $Defaultpattern){
            return sub { 1 };
        }else{
            return sub {
                ($$patt == $_[1]) ? 1 : undef; 
            };
        }
    }else{
        die 'Err: invalid pattern' . Dumper $patt;
    }
}
sub str_pattern {
}

sub matchcompiler {
    my ($body, $patternwrapper , $actionwrapper) = @_;

    if(ref $body  eq 'ARRAY' ){
        my $size = @{ $body };
        my $r = $size % 2 ;
        if($r){
            die "Err: invalid number of patterns: $size"
        }else{
            my @parts = @$body;
            my ($default, @matrix );
            for (my $i = 0 ;  $i < @parts ; $i= $i + 2){
                if($parts[$i] eq '_' ){ $default = 1; }
                push @matrix, [ 
                    $patternwrapper->($parts[$i]), 
                    $actionwrapper->($parts[$i+1] ) ] 
            }

            return ($default)
                ? \@matrix
                : die "Err: no default " 
                ;
        }
    }else{
        die "Err: invalid with body block " 
    }

}

sub matchinterpreter {
    my ($body, $patternwrapper , $actionwrapper) = @_;

    if(ref $body  eq 'ARRAY' ){
        my $size = @{ $body };
        my $r = $size % 2 ;
        if($r){
            die "Err: invalid number of patterns: $size"
        }else{
            my @parts = @$body;
            my ($default, @matrix );
            for (my $i = 0 ;  $i < @parts ; $i= $i + 2){
                if($parts[$i] eq '_' ){ $default = 1; }
                push @matrix, [ 
                    $patternwrapper->($parts[$i]), 
                    $actionwrapper->($parts[$i+1] ) ] 
            }

            return ($default)
                ? \@matrix
                : die "Err: no default " 
                ;
        }
    }else{
        die "Err: invalid with body block " 
    }
}

sub basematcher {
    my ($arg, $body  ) = @_;

    my ($pat_matcher, $action_matcher);
    if(ref \$arg eq 'SCALAR'){
        if(looks_like_number($arg)){
            ($pat_matcher, $action_matcher) =( \&num_pattern , \&base_action)
        }else{
            ($pat_matcher, $action_matcher) =( \&str_pattern , \&base_action)
        }
    }elsif(ref $arg eq 'ARRAY'){
        die 'todo'
    }else {
        die "Err: todo "
    }

    my $action;

    foreach (@$body){
    }
        my ($env, $val) = @_;
        foreach my $row (@$matrix) {
            my ($matches) = $row->[0]->($env, $val);
            if($matches){
                return $row->[1]->($env, $val)
            }
        }

}

sub _matchingcompiler{
        my ($type, $body ) = @_;
        if(ref $arg eq 'TYPE'){
            ::p "jsdf";
        }elsif(ref \$arg eq 'GLOB'){
            my ($r) = *{"main\::$type"}->($arg);
            if($r){
                die 'xx ' . Dumper $r;
            }else{
                die "Err(match) invalid input"
            }
        }else{
            die "Err: (match) not a type"
        }
}

sub matchinginterpreter {
    my ($env, $arg, $body, $type) = @_;

    my $arg_matcher ;
    # check default
    #
    my ($action , $match);

    foreach (@$body){
        my ($checker , $action) = reader($_)
        ($action) = $checker->($arg);
        if($match){
            return $action->($env);
        }

    }
    die "Err: no match";
}
sub matchingcompiler {
    my ($env, $arg, $body, $type) = @_;

    my $arg_matcher ;

    # check default
    #
    my ($action , $match);

    @matrix
    my @matrix;
    foreach (@$body){
        my ($checker , $action) = reader($_)
        push @matrix, [ $checker, $action;

    }
    return 
}

sub match {
    my ($arg, ) = shift;
    my $env = {}; 

    if(@_ == 2){ # user defined types
        my ($type, $body ) = @_;
        my ($res) = _matchinginterpreter($env, $arg, $body, $type);
    }elsif(@_ == 1){    # base types
        my ($body ) = @_;
        my ($res) = _matchinginterpreter($env, $arg, $body);
        return $res;
    }else{
        die "Err: syntax match"
    }
}



sub number_checkpair {
    
    my $val_checker = sub {
        my ($val) = (@_);
        if(ref \$val eq 'SCALAR'){
            return (looks_like_number($val)) ? undef : 
               "Err: value is not a number" ;
        }else{
            return 'Err: val is not a scalar'
        }
    };
    my $pat_checker = sub {
        my ($pat) = @_;
        my $pattype = ref $pat;
        if($pattype eq 'ARRAY'){
            return undef
        }elsif(\$pat eq 'SCALAR'){
            return undef
        }else{
            return "Err: not a valid pattern for numbers"
        }
    };

    return ($val_checker, $pat_checker);
}


sub num{
    @_
}

sub ::with_ (&){
    return ($Id->{withblock}, @_);
}
sub number (&){
    return (\&number_checkpair, @_);
}

sub c {

    my $conser; $conser = sub {
        if(@_ == 0 ){
            return bless [ ], $Cons 
        }elsif(@_ == 1 ){
            return bless [ $_[0]], $Cons
        }elsif(@_ == 2 ){
            return bless [ $_[0], $_[1] ], $Cons 
        }else{
            return bless [ shift,  $conser->(@_) ], $Cons 
        }
    };
    $conser->(@_)
}

sub rtmatch_scalar {
    my ($val, $pat) = @_;

    if(defined $pat  ){
        if (looks_like_number($val)){ # not reliable for stringified numbers due to  perl-bug
            # die "Err: val is not a number" unless looks_like_number $val;
            ($val == $pat) ? 1 : undef
        }else{
            ($val eq $pat) ? 1 : undef
        }
    }else{
        return undef
    } 
}

sub rtmatch_glob{
    die "tdo"
}


sub rtmatch_undef {
    my ($val, $pat) = @_;
    
    (defined $pat)
        ? undef
        : 1;
}


sub rtmatch_dispatcher {
    my ($val) = @_;
    my $type = ref $val;
    my $case =  {
        '' => sub { 
                if(defined $val  ){
                    ( ref \$val eq 'SCALAR' ) ? \&rtmatch_scalar : \&rtmatch_glob
                }else{
                    \&rtmatch_undef
                } },
        _ => sub { die "err " }
    };
    
    (exists $case->{$type} ) 
        ? $case->{$type}->()
        : $case->{_}->();
}

sub glob_wrapper{
    my $glob = shift;
    my %locenv; 
    if(ref \$glob eq 'GLOB'){
         $glob =~  m/main::(.*)/ ;
         my $catch = $1;
         return sub {
             my ($vars) = shift;
             (exists $vars->{$catch}) 
                ? $vars->{$catch}
                : die "Err: variable $catch is not available"
            };
    }else{
        die "var is not a glob"
    }

}

sub base_wrapper {
    my $base_val = shift;
    return sub { $base_val }
}

sub action_wrapper {
    my $action = shift;
    my $type = ref $action;

    my $case =  {
        'CODE' => sub { $action },
        '' => sub { return ( ref \$action eq 'GLOB' ) 
                        ? glob_wrapper($action) 
                        : base_wrapper($action) },
        _ => sub { die "err " }
    };
    
    (exists $case->{$type} ) 
        ? $case->{$type}->()
        : $case->{_}->();

}


sub multimatch {
        die "tdo multimatch"
}
sub exec_action{
    my ($vars, $action ) = @_;
    return $action->($vars);
}
sub domatch {
    my ($vars, $matrix) = (shift, shift);

    if (@_ == 0){
        return $matrix->{default};
    }elsif(@_ == 1){
        my $action =  singlematch($matrix, @_);
        exec_action($vars, $action )
    }else{
        return multimatch($vars, $matrix,  @_)
    }


sub casematch_matrix_ { ## for the compiler
    my $block = shift;
    my @parts = $block->();
    my @rows ;
    my $default;
    for (my $i = 0 ;  $i < @parts ; $i= $i + 2){
        if($parts[$i] eq $Defaultpattern ){
            $default = $parts[ $i + 1 ]
        }else{
            push @rows, [ $parts[$i], action_wrapper($parts[$i+1] ) ] 
        }
    }
    if($default){
            return bless { default => action_wrapper($default), rows => \@rows }, 'MATCHMATRIX'
    }else{ 
        die "Err: match matrix not correct"
    }

}

sub parm_env {

    my @vars;
    foreach  (@_) {
        if(ref \$_ eq 'GLOB'){
              m/main::(.*)/ ;
              push @vars, $1;
        }else{
            die "var is not a glob"
        }
    }

}



sub lex_block_ {
    my ($typechecker, $block) = @_; 


    if(grep { $typechecker eq $_ } ( ) ){
        my @parts = $block->();
        my $size = @parts;
        my $r = $size % 2 ;
        if($r){
            return (undef, "Err: invalid number of patterns")
        }else{
            my $default =  ($parts[$size -2 ]  );
            if($default eq _() ){
                my $ast;
                #my $ast = buildmatrix @parts;
                #$ast->{checker} = $typechecker;
                return $ast;
            }else{
                return (undef, "Err: no default pattern")
            }
        }
    }else{
        return (undef, "Err: invalid with block data " );
    }

}
sub do_match {
    my ($matrix, $val) = @_;

    if($val){
        my $val_matcher = rtmatch_dispatcher($val);
        my $ret;
        foreach my $row (@{ $matrix->{rows}} ){
            my $res = $val_matcher->($val, $row->[0]);
            if($res){
                $ret = $row->[1];
                last;
            }
        }
        exit;
        return ($ret ) ? $ret->({}, $val) : $matrix->{default};
    }else{
        return $matrix->{default};
    }

}


sub get {
    (\&match, \&guard, \&c)
}

