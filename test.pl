use DoubleColon;
use strict;
use warnings;

use Data::Dumper qw(Dumper);

use constant _ => $DoubleColon::_;
(*match, *guard, *c) = DoubleColon::get();

my $i = 333;
match( $i, [
    \333 => sub { print "hi" }, 
    guard(33,  '==', 33)   => sub { print "jsdf" },
#    guard(33,  '==', 33)   => q( print,  "jsdf" ),
#    guard(33,  '==', 33)   => c(print =>  "jsdf" ),
#    guard(33,  '==', 33)   => \33,
#
    _ => sub { print "fuck" }
]);
exit;

__END__
::type color => q{
    Any,   
    { Blue: [ color  ] },
    { Red: [ num  ] },
    { All: [ color ] }, 
};

my ($blue) =  *Any  ;
::match $blue, 'color', [ 
    q{ Red: [a] } => sub { print $_[0]->{a} },
    'Any' => sub { print "fuck"}, 
    (_) => sub{ print "dd " }
] ;

print Dumper $blue;
exit;

::type mixes => q{
    Red,   
    { Color: [ color ] },
};



::d $c;

::match ( $c,  'color')  , [
    'Red'       => q{ return: "hello" }, 
    '[Blue: a]' => sub { print $_[0]->{a} }, 
    '[Blue: a]' => q{ print: a }
];



my $m = ::fun  qw( c color ),   [  
    *Red => sub { print $_[0]->{c} },
    (_) => sub { print "by"}
];


$m->();


;

__END__



c(Blue => [ 33 ] )
Blue(33)

my $icolor ; 
::match $icolor, color => [  

    [ Blue => *a ] => sub { print $_[0]->{a} }
];
print Dumper $c{Red}->(33);

    
#print 'rr ' . Dumper eval $res[0];

__END__
my @comp = $parse->();
print 'cc ' . Dumper @comp; 
#print Dumper $comp[0]->[0]->{'('};

exit;

match 33 , number {
    \'.. a 4' => "suck",
    '*a' => q(print *a) ,
    (_) => "fuck"
};


__END__
match c(33, 44) , with {
    c(*hd, *tl) => 44,
    (_) => 88 
};


my $res = match c(33), with {
    [ *a, *b ] => "fuck off",
    (_)   => "suck"
};

print "hi";
